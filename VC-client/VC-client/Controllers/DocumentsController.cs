﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VC_client.Models;

namespace VC_client.Controllers
{
    [OptionalAuthorize]
    public class DocumentsController : Controller
    {
        // GET: Documents
        public ActionResult Index()
        {
            try
            {
                string status = Request.QueryString["status"];
                string page = String.IsNullOrEmpty(Request.QueryString["page"]) ? "0" : Request.QueryString["page"];
                ViewBag.Status = status;
                ViewBag.NextPage = Convert.ToInt32(page) + 1;

                ViewBag.ShowFile = status == "last" ? "/Documents/Signature" : "/Documents/Edit";
                ViewBag.Dicts = new Dictionary<string, string>() { { "new", "Подписать до" }, { "last", "Подписан" }, { "reject", "Отклонен" } };
                var list = DocumentApi.GetList(status, page);
                int total = 0, lastrow = 0;
                if (list.Count > 0) {
                    total = list[0].Total;
                    lastrow = list[0].LastRow;
                }
                ViewBag.Total = total;
                ViewBag.LastRow = lastrow;
                ViewBag.Documents = list;
                return View();
            }
            catch (WebException ex)
            {
                var r = (HttpWebResponse)ex.Response;
                return new HttpStatusCodeResult((HttpStatusCode)r.StatusCode,
                                   ex.Message);
            } 
        }

        // POST: Documents/Create - создание кода подтверждения
        [HttpPost]
        public ActionResult Create(long id)
        {
            var code = DocumentApi.GetCode(id, System.Web.HttpContext.Current.Session.SessionID);
            return Json(code);
        }

        // GET: Documents/Edit/5 показать файл
        public ActionResult Edit(long id)
        {
            byte[] pdfReportStream = DocumentApi.GetPdf(id);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-length", pdfReportStream.Length.ToString());
            Response.BinaryWrite(pdfReportStream);

           Response.End();
            return View();
        }
        
        // POST: Documents/Edit/5 - подписание документа
        [HttpPost]
        public ActionResult Edit(ConfirmCode cCode)
        {
            try
            {
                DocumentApi.SignInRequestCode(cCode.RequestId, System.Web.HttpContext.Current.Session.SessionID, cCode.Code);
                return Json(new { res = "OK"});
            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Debug.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    if (httpResponse.StatusCode == (HttpStatusCode)422)
                    {
                        return Json(new { error = "error", message = "Неверный код" });
                    }
                    else
                        return Json(new { error = "error", message = ex.Message });
                }
            }
        }
        
        
        // GET: Documents/Signature/5
        public ActionResult Signature(long id)
        {
            byte[] pdfReportStream = DocumentApi.GetSignature(id);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-length", pdfReportStream.Length.ToString());
            Response.BinaryWrite(pdfReportStream);

            Response.End();
            return View();
        }
        
        // POST: Documents/Delete/5 - отклонение документа 
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
