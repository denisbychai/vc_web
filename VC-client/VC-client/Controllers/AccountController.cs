﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VC_client.Models;


namespace VC_client.Controllers
{
    public class AccountController : Controller
    {
        public new RedirectToRouteResult RedirectToAction(string action, string controller)
        {
            return base.RedirectToAction(action, controller);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            LoginModel model = new LoginModel() { UserName = "100000", Password = "0ASDqwe2345^" };
            return View("Login", "_LoginLayout", model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            try
            {

                var res = RestApi.GetApiResponse(
                    url: "api/Account/Login"
                    , method: "post"
                    , obj: model
                );
                JavaScriptSerializer j = new JavaScriptSerializer();
                TokenModels tokenModel = j.Deserialize<TokenModels>(res);

                if ( !String.IsNullOrEmpty(model.LoginCode) && !String.IsNullOrEmpty(tokenModel.access_token) )
                {
                    //Console.Write(Session["Token"]);
                    HttpContext.Session["Token"] = tokenModel.access_token;
                    return RedirectToAction("Index", "Lk");
                }
                model.IsViewCode = true;
                model.DebugCode = tokenModel.code;
                return View("Login", "_LoginLayout", model);
            }
            catch (Exception ex)
            {
                model.ErrorMsg = "Неверные логин/пароль" + ex.Message;
                model.IsViewCode = !String.IsNullOrEmpty(model.LoginCode);
                return View("Login", "_LoginLayout", model);
            }
        }


        [HttpPost]
        [OptionalAuthorize]
        public ActionResult Logout()
        {
            HttpContext.Session["Token"] = null;
            return Json(new { res = "ok" });
        }
    }
}