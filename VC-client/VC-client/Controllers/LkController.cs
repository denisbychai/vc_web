﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VC_client.Models;

namespace VC_client.Controllers
{
    [OptionalAuthorize]
    public class LkController : Controller
    {
        // GET: Lk
        public ActionResult Index()
        {
            try
            {
                return View(new LkEditModel(LkApi.GetUserInfo()));
            }
            catch (WebException ex)
            {
                return RedirectToAction("Login", "Account");
            } 

        }

        // GET: Lk/Edit/5
        public ActionResult Edit()
        {
            try
            {
                return View(new LkEditModel(LkApi.GetUserInfo()));
            }
            catch (WebException ex)
            {
                var r = (HttpWebResponse)ex.Response;
                return new HttpStatusCodeResult((HttpStatusCode)r.StatusCode,
                                   ex.Message);
            }
        }

        // POST: Lk/Edit/5
        [HttpPost]
        public ActionResult Edit(string Id, LkModels collection)
        {
            try
            {
                // TODO: Add update logic here
                var d = LkApi.SaveProfile(collection);
                return Json(new { res = "ok"});
            }
            catch (WebException ex)
            {
                var r = (HttpWebResponse)ex.Response;
                return new HttpStatusCodeResult((HttpStatusCode)r.StatusCode,
                                   ex.Message);
            }
        }

        // POST: Lk/Edit/5
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordBindingModel model)
        {
            try
            {
                // TODO: Add update logic here
                LkApi.ChangePassword(model);
                return Json(new { res = "ok" }, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                var r = (HttpWebResponse)ex.Response;
                return Json(new { error = "error", code = r.StatusCode, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddPhoto(HttpPostedFileBase inputFile)
        {

            if (inputFile == null)
                return Json(new { res = "ok", message = "Отсутствует фотография"});
            try
            {
                BinaryReader sr = new BinaryReader(inputFile.InputStream);
                var model = new LkModels();
                model.Photo = Convert.ToBase64String(sr.ReadBytes(inputFile.ContentLength));
                model.PhotoType = inputFile.ContentType;
                
                LkApi.SaveProfile(model);

            }
            catch (Exception ex)
            {
                return Json( new { res = "error", message = ex.Message });
            }

            return Json(new { res = "ok"});
        }
    }
}
