﻿$(function () {
    $('#editProfile').off('click.lk').on('click.lk', function (e) {
        $.ajax({
            type: 'GET',
            url: '/Lk/Edit',
            data: { status: status }
            , success: function (data) {
                $('#listDocs').html(data);
            }

        });
    })

    $('#changePasswordBtn').off('click.lk').on('click.lk', function (e) {
        if ($('#changePasswordForm').valid()) {
            $.ajax({
                type: 'POST',
                url: '/Lk/ChangePassword',
                data: $('#changePasswordForm').serialize()
                , success: function (data) {
                    if (data.res != 'error') {
                        $('#changePassword').modal('hide');
                        $('#modalSuccessPassword').modal('show');
                        setTimeout(function () { $('#modalSuccessPassword').modal('hide'); }, 1000);
                    } else {
                        show_alert("Ошибка", data.message, null, 'bg-danger');
                    }
                }

            });
        }
    })

    $('#showLk').off('click.lk').on('click.lk', function (e) {
        e.preventDefault();
        var $div = $('div.rightBlock');
        console.log($div.offset());
        var offset = $div.offset();
        var width = $div.outerWidth();
        var height = $div.outerHeight()
        console.log(offset);
        console.log($('#modalShowInfo').offset());
        $('#modalShowInfo').css({ top: offset.top, left: offset.left });
        $('#modalShowInfo').height(height);
        $('#modalShowInfo').width(width);
        $('#modalShowInfo').removeClass('hidden');
    })

    $('#modalShowInfo').off('click.lk').on('click.lk', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#modalShowInfo').addClass('hidden');
    })

    $('#listDocs').off('click.lk', '#saveProfile').on('click.lk', '#saveProfile',  function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/Lk/Edit',
            dataType: 'json',
            data: $('#userProfile').serialize()
            , success: function (data) {
                if (data.error) {
                    console.log('error');
                    show_alert('Ошибка при сохранении', data.message);
                } else {
                    $('#modalSuccessPassword').modal('show');
                    setTimeout(function () { $('#modalSuccessPassword').modal('hide'); $('#editProfile').trigger('click'); }, 1000);
                }
            }
        });
    })

    $('#listDocs').off('click.lk', '#cslProfile').on('click.lk', '#cslProfile', function (e) {
        e.preventDefault();
        show_confirm(
            'Отменить изменения'
            , 'Отменить изменения и выйти из редактирования профиля?'
            , function () {
                //$("#modalСonfirm").modal("hide");
                getDocs('new', $('.menuRow[data-status="new"]'));
            }
        );
    });

    $('#listDocs').off('click.lk', '#changePersonal').on('click.lk', '#changePersonal', function (e) {
        e.preventDefault();
        show_confirm(
            'Изменение данных'
            , 'Вы хотите сообщить об изменениях в учетных данных (номер телефона, ФИО, паспортные данные)? После отправки запроса, с Вами свяжется специалист Колл-центра. Личный кабинет будет заблокирован.'
            , function () {
                console.log('API CALL to block and send request to CUOK');
                //$("#modalСonfirm").modal("hide");
                //getDocs('new', $('.menuRow[data-status="new"]'));
            }
        );
    });

    $('#listDocs').off('click.lk', '#changePasswordModal').on('click.lk', '#changePasswordModal', function (e) {
        e.preventDefault();
        $('#changePasswordForm')[0].reset();
        $('#changePassword').modal('show');
    });

})