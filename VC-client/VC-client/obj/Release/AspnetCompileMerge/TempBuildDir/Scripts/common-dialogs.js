﻿//функция обратного отсчета для повторного отправления кода подтверждения 
function countDown(second) {
    console.log('countdown');
    var now = new Date();
    second = (arguments.length == 1) ? second + now.getSeconds() : second;
    endYear = now.getFullYear();
    endMonth = now.getMonth();  //номер месяца начинается с 0
    endDay = now.getDate();
    endHour = now.getHours();
    endMinute = now.getMinutes();
    //добавляем секунду к конечной дате (таймер показывает время уже спустя 1с.)
    var endDate = new Date(endYear, endMonth, endDay, endHour, endMinute, second + 1);
    console.log(endDate);
    var interval = setInterval(function () { //запускаем таймер с интервалом 1 секунду
        var time = endDate.getTime() - now.getTime();
        console.log(time);
        if (time < 0) {                      //если конечная дата меньше текущей
            clearInterval(interval);
            alert("Неверная дата!");
        } else {
            var seconds = Math.floor(time / 1e3) % 60;
            $('#confirmTimeout').text('До отправки нового кода осталось:' + seconds + ' сек');
            if (!seconds) {
                clearInterval(interval);
                $('#confirmTimeout').css('display', 'none');
                $('#sendCodeBtn').attr('disabled', false);
            }
        }
        now.setSeconds(now.getSeconds() + 1); //увеличиваем текущее время на 1 секунду
    }, 1000);
}

//показать окно подписи пакета
function show_signing(request_id) {
    $('#sendCode').val('');
    $('#sendCodeBtn').data('requestid', request_id);
    $('#modalSigning').modal('show');
}

//показать подтверждение
function show_confirm(title, text, ok_callback, cancel_callback) {
    var modal = $("#modal-confirm"),
        dialog = modal.find('.modal-dialog');
    modal.find(".modal-title").html(title);
    modal.find(".modal-body").html(text);

    if (title) {
        modal.find('.modal-header').removeClass('empty');
    } else {
        modal.find('.modal-header').addClass('empty');
    };

    modal.find(".modal-body").html('<div class="row"><div class="col-sm-1"><span class="fa fa-question-circle fa-2x"></span></div><div class="col-sm-11" style="margin-top:4px">' + text + '</div></div>');

    $("#modal-confirm .confirm-cancel").off("click");
    $("#modal-confirm .confirm-cancel").on("click", function () {
        $("#modal-confirm").modal("hide");
    });

    $("#modal-confirm .confirm-ok").off("click");
    $("#modal-confirm .confirm-ok").on("click", function () {
        if ($.isFunction(ok_callback)) {
            ok_callback();
        }
        var modal = $("#modal-confirm"),
            $form = modal.find('form');
        if ($form.length == 0)
            modal.modal("hide");
    });

    modal.css('display', 'block');
    dialog.css("margin-top", Math.max(0, ($(window).height() * 2 / 3 - dialog.height()) / 2));
    modal.modal("show");
}

//показать alert
function show_alert(title, text, ok_callback, header_class) {
    var modal = $("#modalAlert"), dialog = modal.find('.modal-dialog'), header_class = (header_class) ? header_class : 'bg-danger';
    modal.find('.modal-content').addClass(header_class);
    modal.find('.modal-header').addClass(header_class);
    modal.find(".modal-title").html(title);
    modal.find(".modal-body").html('<div class="row"><div class="col-sm-1"><span class="fa fa-exclamation-circle fa-2x"></span></div><div class="col-sm-11" style="margin-top:4px">' + text + '</div></div>');

    if (title) {
        modal.find('.modal-header').removeClass('empty');
    } else {
        modal.find('.modal-header').addClass('empty');
    };

    $("#modal-alert .alert-ok").off("click");
    $("#modal-alert .alert-ok").on("click", function () {
        if ($.isFunction(ok_callback)) {
            ok_callback();
        }
        modal.modal("hide");
        modal.find('.modal-content').removeClass(header_class);
        modal.find('.modal-header').removeClass(header_class);
    });

    modal.css('display', 'block');
    dialog.css("margin-top", Math.max(0, ($(window).height() * 2 / 3 - dialog.height()) / 2));
    modal.modal("show");
}

//получение списка пакетов документов
function getDocs(status, $elem) {
    $.ajax({
        type: 'GET',
        url: '/Documents/Index',
        data: { status: status }
        , success: function (data) {
            $('#listDocs').html(data);
            $('#modalSigning').modal('hide');
            $elem.addClass('menuRowActive');
        }
    });
}

$(function () {
    //Общая обработка ошибок ajax
    $(document).ajaxError(function (e, xhr, settings) {
        if (xhr.status == 401) {
            location = '@Url.Action("Login", "Account" )';
        } else {
            show_alert("Ошибка", xhr.responseText, null, 'bg-danger');
        }
    });

    //Валидация формы изменения пароля
    $("#changePasswordForm").validate({
        rules: {
            OldPassword: "required",
            NewPassword: {
                required: true,
                minlength: 6
            },
            ConfirmPassword: {
                required: true,
                minlength: 6,
                equalTo: "#NewPassword"
            }
        },
        messages: {
            OldPassword: "Пожалуйста, введите старый пароль",
            NewPassword: {
                required: "Пожалуйста, введите новый пароль",
                minlength: "Пароль должен быть не короче 6 символов"
            },
            ConfirmPassword: {
                required: "Пожалуйста, введите новый пароль",
                minlength: "Пароль должен быть не короче 6 символов",
                equalTo: "Пароль и подтверждение не совпадают"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            error.insertAfter(element);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });

    //переход между типами документов
    $('.menuRow').off('click.lk')
        .on('click.lk', function (e) {
            console.log('click');
            e.preventDefault();
            $('.menuRow').removeClass('menuRowActive');
            getDocs($(this).data('status'), $(this));
        })
        .hover(function () {
            $(this).css('cursor', 'pointer');
        })

    
    //выйти из системы
    $('#logout').off('click.lk').on('click.lk', function (e) {
        e.preventDefault();
        show_confirm("Выход", 'Вы уверены, что хотите выйти?', function () {
            $.ajax({
                type: 'POST',
                url: '/Account/Logout'
                , success: function (data) {
                    window.location = '/Account/Login';
                }
            });
        })
    });
    //при закрытии подписного окна, перезагрузим список "новых" пакетов
    $('#modalSigning').on('hidden.bs.modal', function (e) {
        getDocs('new', $('.menuRow[data-status="new"]'));
    })

    //маска на ввод подтверждающего кода
    $('#sendCode').inputmask("999999");
    $('#loginCode').inputmask("999999");

    //прогрузим сразу же "новые"
    getDocs('new', $('.menuRow[data-status="new"]'));
    $('#newDocs').addClass('btn-menu-active');

    $('#loadPhotoBtn').off('click.lk').on('click.lk', function (e) {
        e.preventDefault();
        var property = $('#inputPhoto').prop('files')[0];
        console.log(property);
        var image_name = property.name;
        var image_extension = image_name.split('.').pop().toLowerCase();

        if ($.inArray(image_extension, ['gif', 'jpg', 'jpeg', 'png']) == -1) {
            show_alert('Ошибка', "Формат файла должен быть: 'gif', 'jpg', 'jpeg', 'png'");
            return false;
        }

        if (property.size <= 0) {
            show_alert('Ошибка', "Размер файла не может быть равен 0");
            return false;
        }

        var form_data = new FormData();
        form_data.append("inputFile", property);
        $.ajax({
            type: 'POST',
            url: '/Lk/AddPhoto',
            data: form_data,
            processData: false, 
            cache: false,
            contentType: false,
            success: function (data) {
                window.location = '/Lk/Index';
            }
        });
    });
})