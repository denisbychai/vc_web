﻿$(function () {
    //запросим код подтверждения для подписи пакета
    $('#sendCodeBtn').off('click.requests').on('click.requests', function (e) {
        var self = this;
        $.ajax({
            type: 'POST',
            datatype: 'json',
            url: '/Documents/Create/' + $(self).data('requestid')
            , success: function (data) {
                console.log(data);
                $('#confirmTimeout').css('display', 'block');
                $('#sendCodeBtn').attr('disabled', true);
                countDown(30);
            }

        });
    })

    //Отправим пакет на подпись
    $('#sendSigning').off('click.requests').on('click.requests', function (e) {
        var code = $('#sendCode').inputmask('unmaskedvalue');;
        console.log(code);
        if (code.length != 6) {
            show_alert('Ошибка!', 'Введите код подтверждения');
            return false;
        }

        var self = this;
        $.ajax({
            type: 'POST',
            datatype: 'json',
            url: '/Documents/Edit/',
            data: { RequestId: $('#sendCodeBtn').data('requestid'), Code: code }
            , success: function (data) {
                if (data.error) {
                    show_alert('Ошибка!', data.message);
                } else {
                    $('#modalSigning').modal('hide');
                }
            }

        });
    })

    //следующая страница пкетов
    $('#listDocs').off('click.docslist', '.nextpage').on('click.doclist', '.nextpage', function (e) {
        e.preventDefault();
        var self = $(this);
        $.ajax({
            type: 'GET',
            url: '/Documents/Index',
            data: { status: self.data('status'), page: self.data('nextpage')}
            , success: function (data) {
                $('#listDocs').html(data);
                $('#modalSigning').modal('hide');
            }
        });
    });

    //подписать пакет
    $('#listDocs').off('click.docslist', '.signDoc').on('click.docslist', '.signDoc', function (e) {
        e.stopPropagation();
        e.preventDefault();
        show_signing($(this).data('requestid'));
    })

    //отклонить пакет
    $('#listDocs').off('click.docslist', '.rjctDoc').on('click.docslist', '.rjctDoc', function (e) {
        e.stopPropagation();
        e.preventDefault();
        show_confirm('Отклонить документ', 'Вы отказываетесь подписать данный пакет документов?');
    })

})