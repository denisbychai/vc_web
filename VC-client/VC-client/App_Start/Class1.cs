﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VC_client
{
    public class OptionalAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Session["Token"] != null && !String.IsNullOrEmpty(httpContext.Session["Token"].ToString());
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {

                filterContext.Result = new HttpStatusCodeResult(403, "Forbidden");
            }
            else
            {
                // Returns HTTP 401 - Assuming they losted access while on the proper page.
                filterContext.Result = new HttpUnauthorizedResult();
                //This solution will only work on Asp.net 4.5 and update!
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                }
            }
        }
    }

}