﻿using System.Web;
using System.Web.Optimization;

namespace VC_client
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            
           // bundles.IgnoreList.Clear();
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // готово к выпуску, используйте средство сборки по адресу https://modernizr.com, чтобы выбрать только необходимые тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"
                      , "~/Content/Phone.css"
                      , "~/Content/Login.css"
                      , "~/Content/Desktop.css"
                      , "~/Content/Portrait.css"));
            bundles.Add(new ScriptBundle("~/bundles/inputmask").Include(
                  "~/Scripts/inputmask.js", "~/Scripts/inputmask.extensions.js", "~/Scripts/inputmask.numeric.extensions.js", "~/Scripts/inputmask.jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/common-site").Include(
                      "~/Scripts/common-dialogs.js",
                      "~/Scripts/lk-manage.js",
                       "~/Scripts/package-manage.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
