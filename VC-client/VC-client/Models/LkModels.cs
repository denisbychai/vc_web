﻿using System;
using System.Web.Script.Serialization;

namespace VC_client.Models
{
    
    public class LkModels
    {

            public string Id { get; set; }
            public string PhoneNumber { get; set; }
            public string Email { get; set; }
            public string UserName { get; set; }
            public string RoleName { get; set; }
            public string RoleId { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Patronymic { get; set; }
            public string SexName { get; set; }
            public DateTime? BirthDate { get; set; }
            public string RegionRegistration { get; set; }
            public string Pep { get; set; }
            public string DocumentTypeName { get; set; }
            public string DocumentSerial { get; set; }
            public string DocumentNumber { get; set; }
            public DateTime? DocumentIssuedDate { get; set; }
            public string DocumentIssuedCode { get; set; }
            public string DocumentIssuedBy { get; set; }
            public string UserId { get; set; }
            public string Workplace { get; set; }
            public string Position { get; set; }
            public string ExtPhone { get; set; }
            public string Vkontakte { get; set; }
            public string Facebook { get; set; }
            public int CountRequests { get; set; }
            public string Photo { get; set; }
            public string PhotoType { get; set; }
    }

    public class LkEditModel
    {
        public string Id { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Fio { get; set; }
        public string RegionRegistration { get; set; }
        public string SerialNumber { get; set; }
        public string DocumentIssued { get; set; }
        public string Workplace { get; set; }
        public string Position { get; set; }
        public string ExtPhone { get; set; }
        public string Vkontakte { get; set; }
        public string Facebook { get; set; }
        private string isEmpty(string s) {
            var d = String.IsNullOrEmpty(s) ? "" : s.Trim();
            return String.IsNullOrEmpty(d) ? "&nbsp;" : d;
        }
        public int CountRequests { get; set; } = 0;
        public string PhotoUrl { get; set; }


        public LkEditModel(LkModels model)
        {
            PhoneNumber = isEmpty(model.PhoneNumber);
            Email       = isEmpty(model.Email);
            Fio         = isEmpty(model.Surname + " " + model.Name + " " + model.Patronymic);
            RegionRegistration = isEmpty(model.RegionRegistration);
            DocumentIssued = isEmpty(model.DocumentIssuedDate + " " + model.DocumentIssuedBy);
            SerialNumber = isEmpty(model.DocumentSerial + " " + model.DocumentNumber);
            Workplace = model.Workplace;
            Position = model.Position;
            ExtPhone = model.ExtPhone;
            Vkontakte = model.Vkontakte;
            Facebook = model.Facebook;
            Id = model.Id;
            CountRequests = model.CountRequests;
            PhotoUrl = !String.IsNullOrEmpty(model.Photo) ? ("data:" + model.PhotoType + ";base64," + model.Photo) : "/Images/noavatar.png";
        }
    } 

    public class LkApi {
        static public LkModels GetUserInfo() {
            JavaScriptSerializer j = new JavaScriptSerializer();
            var f = RestApi.GetApiResponse("/api/lk", "GET");
            return j.Deserialize<LkModels>(f);
        }

        static public LkModels SaveProfile(LkModels collection)
        {
            JavaScriptSerializer j = new JavaScriptSerializer();
            return j.Deserialize<LkModels>(RestApi.GetApiResponse(url:"/api/Lk", method:"PUT", obj: collection));
        }

        static public void ChangePassword(ChangePasswordBindingModel collection)
        {
            JavaScriptSerializer j = new JavaScriptSerializer();
            RestApi.GetApiResponse(url: "api/Account/ChangePassword", method: "POST", obj: collection);
        }

    }

    public class ChangePasswordBindingModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}