﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VC_client.Models
{
    public class TokenModels
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string userName { get; set; }
        public string code { get; set; }
    }
}