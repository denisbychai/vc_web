﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace VC_client.Models
{
    public class LoginBind
    {
        public string grant_type { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class LoginModel
    {
        private string _ErrorMsg;
        private string _ButtonTextGet = "ПОЛУЧИТЬ КОД";
        private string _ButtonTextLogin = "ВОЙТИ";
        public string UserName { get; set; }
        public string Password { get; set; }
        public string LoginCode { get; set; }
        public string ButtonText {
            get { return !IsViewCode ? _ButtonTextGet : _ButtonTextLogin; }
        }
        public bool IsViewCode { get; set; }
        public string DebugCode { get; set; }
        public bool IsShowError { get; set; }
        public string ErrorMsg {
            get { return _ErrorMsg; }
            set {
                IsViewCode = true;
                IsShowError = true;
                _ErrorMsg = value;
            }
        }
    }

    public class LoginModels
    {
        static public bool ValidateUser(string UserName, string Password)
        {

            var request = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["APIUrl"] + "api/Account/Login");

            var postData = "username=" + UserName;
            postData += "&password=" + Password;
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            HttpWebResponse response;

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                return false;
            }

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            JavaScriptSerializer j = new JavaScriptSerializer();
            TokenModels model = j.Deserialize<TokenModels>(responseString);
            HttpContext.Current.Session["Token"] = model.access_token;
            return true;
        }
    }


}