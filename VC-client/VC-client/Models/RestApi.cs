﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace VC_client.Models
{
    public class RestApi
    {
        public static string GetApiResponse(string url, string method, string content_type = "application/json; charset=utf-8", Object obj = null, bool auth = true) {
            if (obj != null && method == "GET") {

                int i = 0;
                foreach(var prop in obj.GetType().GetProperties()) {
                    if (i == 0)
                        url += "?";
                    else
                        url += "&";
                    url += prop.Name + "=" + prop.GetValue(obj, null).ToString();
                    i++;
                }
            }
            var request = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["APIUrl"] + "/" +  url);

            request.Method = method;
            request.ContentType = content_type;
            if (auth)
            {
                request.Headers["Token"] = System.Configuration.ConfigurationManager.AppSettings["APIToken"];
                request.Headers["Authorization"] = HttpContext.Current.Session["Token"] == null ? "" : ("Bearer " + HttpContext.Current.Session["Token"].ToString());
            }

            if (obj != null && method != "GET") {
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(new JavaScriptSerializer().Serialize(obj));
                }
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
        
    }
}