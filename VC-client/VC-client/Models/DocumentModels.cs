﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace VC_client.Models
{

    public class Package {
        public long Id { get; set; }
        public string Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string Bank { get; set; }
        public int Doccnt { get; set; }
        public int Total { get; set; }
        public int LastRow { get; set; }
    }

    public class DocumentsList
    {
        public long Id { get; set; }
        public string Status { get; set; }
        public DateTime FileDate { get; set; }
        public int? Number { get; set; }
        public string ShortName { get; set; }
    }

    public class ListPackageView : Package
    {
        public List<DocumentsList> Docs { get; set; }
    }

    public class DocumentModels
    {

        public string Id { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public string RoleId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string SexName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string RegionRegistration { get; set; }
        public string Pep { get; set; }
        public string DocumentTypeName { get; set; }
        public string DocumentSerial { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime? DocumentIssuedDate { get; set; }
        public string DocumentIssuedCode { get; set; }
        public string DocumentIssuedBy { get; set; }
        public string UserId { get; set; }
    }

    public class ConfirmCode
    {
        public long RequestId { get; set; }
        public string SessionId { get; set; }
        public string UserId { get; set; }
        public string Code { get; set; }
    }

    public class GetPdf {
        public string file { get; set; }
    }


    public class DocumentApi
    {
        static public List<ListPackageView> GetList(string status, string page)
        {
            JavaScriptSerializer j = new JavaScriptSerializer();
            var listPack = j.Deserialize<List<ListPackageView>>(RestApi.GetApiResponse(url: "/api/request", method: "GET", obj: new { Status = status, Page = page }));
            for (int i = 0; i < listPack.Count; i++) {
                listPack[i].Docs = GetDocumentList(listPack[i].Id);

            }
            return listPack;
        }

        static public List<DocumentsList> GetDocumentList(long id)
        {
            JavaScriptSerializer j = new JavaScriptSerializer();
            var answer = RestApi.GetApiResponse("/api/request/" + Convert.ToString(id), "GET");
            return j.Deserialize<List<DocumentsList>>(answer);
        }

        static public byte[] GetPdf(long id)
        {
            JavaScriptSerializer j = new JavaScriptSerializer();
            j.MaxJsonLength = Int32.MaxValue;
            var answerStr = RestApi.GetApiResponse("/api/RequestFile/" + Convert.ToString(id), "GET");
            GetPdf answer = j.Deserialize<GetPdf>(answerStr);
            if (answer == null || answer.file == null)
                return new byte[0];
            else
                return Convert.FromBase64CharArray(answer.file.ToCharArray(), 0, answer.file.Length);
        }

        static public string GetCode(long requestid, string SessionId) {
            return RestApi.GetApiResponse(url: "/api/ConfirmCode/", method: "POST", obj: new { RequestId= requestid, SessionId= SessionId });
        }

        static public void SignInRequestCode(long requestid, string SessionId, string code) {
            RestApi.GetApiResponse(url: "/api/ConfirmCode/", method: "PUT", obj: new { RequestId = requestid, SessionId = SessionId, Code = code });
            RestApi.GetApiResponse(url: "/api/Request/", method: "PUT", obj: new { RequestId = requestid, Status = "SignIn", Code = code });
        }

        static public byte[] GetSignature(long id)
        {
            JavaScriptSerializer j = new JavaScriptSerializer();
            j.MaxJsonLength = Int32.MaxValue;
            var answerStr = RestApi.GetApiResponse("/api/RequestSignature/" + Convert.ToString(id), "GET");
            GetPdf answer = j.Deserialize<GetPdf>(answerStr);
            if (answer == null)
                return new byte[0];
            else
                return Convert.FromBase64CharArray(answer.file.ToCharArray(), 0, answer.file.Length);
        }
    }
}